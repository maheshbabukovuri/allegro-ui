FROM node:10
WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install --production
COPY . .

EXPOSE 5000
EXPOSE 5010

CMD ["npm", "start"]
